﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDB;
using Base_methods;

using System.Xml.Linq;
using System.IO;

namespace ToDB
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// 

		public static bool InvalidArgs;

		[STAThread]
		static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);


			Main form = new Main();
			Menu form2 = new Menu();
			Base b;
			try{
				b = new Base("dbxml.xml", false);
			}catch(Exception Ex){
				Console.WriteLine(String.Format("Unspecified Exception!\r\n{0}\r\nExiting...",Ex.Message));
				return;
			}
			if(!Base_methods.toDB.CheckDatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword))){
				Base_methods.toDB.Createdatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword));
				if(!Base_methods.toDB.CheckDatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword))){
					throw(new Exception("Error creating database!"));
				}else{}
			}else{

			}
			
			b.open();
			form.b = b;
			
			List<string> elementlist = new List<string>();

			StreamReader sr;

			if (args.Length == 1){
				try{
					Console.WriteLine(string.Format("Loading file {0}", args[0]));
					sr = new StreamReader(args[0]);
					XElement xt = XElement.Load(sr);
					foreach(XElement element in xt.Descendants("elements").Descendants()){
						Console.WriteLine(string.Format("--> Adding Element: {0}",element.Name.ToString()));
						elementlist.Add(element.Name.ToString());
					}

					Console.WriteLine("Obtaining Elements from XML");
					b.Get(elementlist, true, null);
					Console.WriteLine("All elements from XML have been put into the DataBase!");
				}catch(FileNotFoundException Ex){
					Console.WriteLine(string.Format("File {0} Not found! Exiting...", Ex.FileName));
				}catch(Exception Ex){
					Console.WriteLine(String.Format("Unspecified Exception!\r\n{0}\r\nExiting...",Ex.Message));
				}finally{
					InvalidArgs = true;
				}

			}if(args.Length > 1){
				Console.WriteLine("Please specify only one file at at time please. Exiting...");
			}if (InvalidArgs){
				Environment.Exit(-1);
			}else{
				Application.Run(form2);
			}
		}
	}
}
