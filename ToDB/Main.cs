﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Base_methods;
using System.Xml.Linq;
using System.Threading;

namespace ToDB
{
	public partial class Main : Form
	{
		public Base b;
		public Main()
		{
			InitializeComponent();
			//string dbname = "GFI";
			try{
				b = new Base("dbxml.xml", false);
			}catch(Exception Ex){
				MessageBox.Show(Ex.Message + "\r\n" + Ex.Data);
				return;
			}
			if(!Base_methods.toDB.CheckDatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword))){
				Base_methods.toDB.Createdatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword));
				if(!Base_methods.toDB.CheckDatabase(b.SQLdatabasename, string.Format("{0};{1};{2};database=master", b.SQLserver, b.SQLusername, b.SQLpassword))){
					throw(new Exception("Error creating database!"));
				}else{}
			}else{

			}
			b.open();
			button2.Enabled = false;
		}

		private void button2_Click(object sender, EventArgs e)
		{
				List<string> itemlist = new List<string>();
			if(!backgroundWorker1.IsBusy){
				if(checkBox2.Checked == true){
				foreach(Object obj in comboBox1.Items){
					itemlist.Add(obj.ToString());
				}
				}else{
					itemlist.Add(comboBox1.Text);
			}
				progressBar1.Value = 0;
				backgroundWorker1.RunWorkerAsync(itemlist);
				
			}else{
				MessageBox.Show("Please wait till the current Fetch has finished...");
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			b.Config = XElement.Load(textBox1.Text);
			XElement list = (b.Config.Element("elements"));

			button1.Enabled = false;
            comboBox1.Items.Clear();
			
			foreach(XElement elements in list.Elements()){
					if(elements.Name == "hidden"){
					}else{
						comboBox1.Items.Add(elements.Name.ToString());
				}
			}
			comboBox1.SelectedIndex = 0;
			button1.Enabled = true;
			button2.Enabled = true;
		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			//Console.WriteLine("Backgroundworker process spawned with id: " + Thread.CurrentThread.ManagedThreadId);
			List<string> items = (List<string>)e.Argument;
			b.Get(items, checkBox1.Checked, sender as BackgroundWorker);
		}

		private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
				progressBar1.Value = 100;
				MessageBox.Show("Fetching Completed!");
		}

		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			label1.Text = "Current Progress: " + e.ProgressPercentage + "%";
			progressBar1.Value = e.ProgressPercentage;
		}
	}
}