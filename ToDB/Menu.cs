﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace ToDB
{
	public partial class Menu : Form
	{
		Main form;
		ProcessStartInfo info = new ProcessStartInfo();
		public Menu()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			form = new Main();
			this.Hide();
			form.Show();
			form.FormClosed += new System.Windows.Forms.FormClosedEventHandler(Form_FormClosed);
			form.Activate();
		}

		private void Form_FormClosed(object sender, FormClosedEventArgs e)
		{
			this.Show();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Hide();
			info.CreateNoWindow = true;
			info.RedirectStandardOutput = true;
			info.UseShellExecute = false;
			info.FileName = "ToDB.exe";
			info.Arguments = "min.xml";

			string output;

			using (Process exeProcess = Process.Start(info))
			{
				StreamReader myStreamReader = exeProcess.StandardOutput;
				while(!exeProcess.HasExited){
					Console.WriteLine(myStreamReader.ReadLine());
				}
			}
			this.Hide();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Hide();
			info.CreateNoWindow = true;
			info.RedirectStandardOutput = true;
			info.UseShellExecute = false;
			info.FileName = "ToDB.exe";
			info.Arguments = "max.xml";

			string output;

			using (Process exeProcess = Process.Start(info))
			{
				StreamReader myStreamReader = exeProcess.StandardOutput;
				while(!exeProcess.HasExited){
					Console.WriteLine(myStreamReader.ReadLine());
				}
			}
			this.Show();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			this.Hide();
			Application.Exit();
		}
	}
}
